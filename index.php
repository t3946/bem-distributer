<?php

require "Bem.php";

$rules = <<<CSS
.box {
  display: block;
}

.box:hover {
    display: none;
}

.box[class="box"] {
    display: flex;
}
CSS;

try {
    Bem::saveAsBem( $rules );
} catch ( Exception $e ) {
    echo 'FATAL ERROR: ' . $e->getMessage() . PHP_EOL;
}
