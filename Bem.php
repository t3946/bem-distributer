<?php

class Bem
{
    private const EF_INVALID_SELECTOR = 'Invalid selector "%s"';
    private const EF_UNKNOWN_BLOCK = 'Unknown block %s';
    private const OUTPUT_DIR = __DIR__ . DIRECTORY_SEPARATOR . 'blocks';

    private static array $update_files = [];
    private static array $bem;

    //TODO: это уже для :hover и и т.п. т.к. там больше однго правила в файле, надо дозаписывать
    private static function writeToFile( string $filename, string $content ): void
    {
        //rewrite
        if ( in_array( $filename, self::$update_files, true ) ) {
            file_put_contents( $filename, $content );
            $update_files[] = $filename;
            return;
        }

        //update
        $old_content = file_get_contents($filename);
        file_put_contents( $filename, $content . $old_content . "\n" );
    }

    private static function validateSelector( string $selector ): bool
    {
        return preg_match( '/^\.[^_]+$/', $selector ) !== false
            || preg_match( '/^\.[^_]+__[^_]+$/', $selector ) !== false
            || preg_match( '/^\.[^_]+(?:_[^_]+){1,2}$/', $selector ) !== false;
    }

    private static function saveBlock( array $block ): void
    {
        $block_name = $block[ 'block_name' ];
        $dir = self::OUTPUT_DIR . DIRECTORY_SEPARATOR . $block_name;
        is_dir( $dir ) ?: mkdir( $dir );
        //save styles
        $filename = $dir . DIRECTORY_SEPARATOR . "$block_name.css";
        file_put_contents( $filename, $block[ 'rule' ] . "\n" );
    }

    private static function saveElement( array $element ): void
    {
        //check block on exists
        $block_dir = self::OUTPUT_DIR . DIRECTORY_SEPARATOR . $element[ 'block_name' ];
        $block_filename = $block_dir . DIRECTORY_SEPARATOR . "{$element['block_name']}.css";

        if ( !file_exists( $block_filename ) ) {
            throw new Exception( self::EF_UNKNOWN_BLOCK, $element[ 'block_name' ] );
        }

        //make element directory
        $element_postfix = "__{$element[ 'element_name' ]}";
        $element_dir = $block_dir . DIRECTORY_SEPARATOR . $element_postfix;
        is_dir( $element_dir ) ?: mkdir( $element_dir );

        //save element css
        $element_filename = $element_dir . DIRECTORY_SEPARATOR . "{$element[ 'entity_name']}.css";
        file_put_contents( $element_filename, $element[ 'rule' ] . "\n" );
    }

    private static function saveModifier( array $modifier ): void
    {
        //check block on exists
        $block_dir = self::OUTPUT_DIR . DIRECTORY_SEPARATOR . $modifier[ 'block_name' ];
        $block_filename = $block_dir . DIRECTORY_SEPARATOR . "{$modifier['block_name']}.css";

        if ( !file_exists( $block_filename ) ) {
            throw new Exception( self::EF_UNKNOWN_BLOCK, $modifier[ 'block_name' ] );
        }

        //make modifier directory
        $modifier_postfix = "_{$modifier[ 'modifier_name' ]}";
        $modifier_dir = $block_dir . DIRECTORY_SEPARATOR . $modifier_postfix;
        is_dir( $modifier_dir ) ?: mkdir( $modifier_dir );

        //save modifier css
        $filename = "{$modifier[ 'entity_name' ]}.css";
        $element_filename = $modifier_dir . DIRECTORY_SEPARATOR . $filename;
        file_put_contents( $element_filename, $modifier[ 'rule' ] . "\n" );
    }

    private static function save(): void
    {
        array_walk( self::$bem[ 'blocks' ], [ __CLASS__, 'saveBlock' ] );
        array_walk( self::$bem[ 'modifiers' ], [ __CLASS__, 'saveModifier' ] );
        array_walk( self::$bem[ 'elements' ], [ __CLASS__, 'saveElement' ] );
    }

    private static function parse( string $rules ): void
    {
        preg_match_all( '/\..*?{[^}]*?}/m', $rules, $matches );

        array_walk( $matches[ 0 ], static function ( string $rule ) {
            preg_match( '/(.*?){/', $rule, $matches );
            $selector = trim( $matches[ 1 ], ' ' );

            if ( self::validateSelector( $selector ) === false ) {
                throw new Exception( sprintf( self::EF_INVALID_SELECTOR, $selector ) );
            }

            $entity_name = trim( $selector, '.' );
            $parts = explode( '_', $entity_name );

            // block
            if ( strpos( $entity_name, '_' ) === false ) {
                self::$bem[ 'blocks' ][] = [
                    'entity_name' => $parts[ 0 ],
                    'block_name' => $parts[ 0 ],
                    'rule' => $rule,
                ];
                return;
            }

            //element
            if ( strpos( $entity_name, '__' ) !== false ) {
                self::$bem[ 'elements' ][] = [
                    'entity_name' => $entity_name,
                    'block_name' => $parts[ 0 ],
                    'element_name' => $parts[ 2 ],
                    'rule' => $rule,
                ];
                return;
            }

            //modifier
            self::$bem[ 'modifiers' ][] = [
                'entity_name' => $entity_name,
                'block_name' => $parts[ 0 ],
                'modifier_name' => $parts[ 1 ],
                'modifier_value' => $parts[ 2 ],
                'rule' => $rule,
            ];
        } );
    }

    public static function saveAsBem( string $rules ): void
    {
        self::$bem = [
            'blocks' => [],
            'elements' => [],
            'modifiers' => [],
        ];

        self::parse( $rules );
        self::save();
    }
}
